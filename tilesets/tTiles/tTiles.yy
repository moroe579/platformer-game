{
    "id": "9e1b7966-f335-4221-a2b3-4e1351ba4d1c",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tTiles",
    "auto_tile_sets": [
        {
            "id": "fa02de4d-924f-43a3-bf4a-327d8014fa4f",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_1",
            "tiles": [
                47,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
                28,
                29,
                30,
                31,
                32,
                33,
                34,
                35,
                36,
                37,
                38,
                39,
                40,
                41,
                42,
                43,
                44,
                45,
                46
            ]
        },
        {
            "id": "2754c7a5-7873-4207-84f4-ce730da6162f",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_2",
            "tiles": [
                47,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 7,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "e1d131d8-1ac5-48a4-b66d-fbebefafb1c2",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
            36,
            37,
            38,
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            46,
            47
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 48,
    "tileheight": 64,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 64,
    "tilexoff": 0,
    "tileyoff": 0
}