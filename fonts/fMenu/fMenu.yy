{
    "id": "707d551e-ea11-484d-abf6-f2c4cdb4d3d4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 5,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "American Captain",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b13a287b-5192-47dd-a045-69bb234e07e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4ab82a36-1131-456a-971d-ed9b7e6cf313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 226,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3366585a-278c-422a-9b57-6a57da71b879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 214,
                "y": 119
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c482454c-42f8-48fd-b9d1-044206b1042b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 197,
                "y": 119
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f79ed498-8f8b-41a5-ac12-27f28c7cbf0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 184,
                "y": 119
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8e18598c-b6f2-4c13-88de-620515aa4680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 160,
                "y": 119
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5df2d00c-b16e-4d2f-bf87-388f4c2210d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 141,
                "y": 119
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c2c0fecd-2b4d-4368-aff7-6da17605bf66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 134,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "59a32c27-72a5-4b93-a2b1-c9c4bb4e128d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 119
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a79593bb-ad3f-4fb9-a9dc-5bf0e30a7920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 113,
                "y": 119
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ff10bd3f-6c21-4671-bfd7-a2026cb31e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 119
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "84a73c1d-20b1-4236-bf68-f47c5979c1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 96,
                "y": 119
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "83a78369-d685-4edd-a99c-b3705e0a737a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 74,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7558c184-d81e-454b-8f40-7cc648d36e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 63,
                "y": 119
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5c48d199-84db-4b46-b5d0-78a60ecd1ef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 55,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "15059395-5450-4b80-99ca-01b9e505a305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 42,
                "y": 119
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e13728b8-05d2-4a2e-8523-45cfe09e2db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 27,
                "y": 119
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ee8ef1be-6011-4f8c-9329-497829ce3abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 16,
                "y": 119
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "04d18d2b-2d97-497b-8fd6-dc9c48a107b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ccfe78d5-de65-41a8-8904-31479f86a7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 230,
                "y": 80
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "142f7ff8-ff81-47e3-bbda-19bf875355ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 214,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ba799457-8a86-40d0-a5cd-aa4e00ccbba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 82,
                "y": 119
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3b4a9da2-e050-478f-9843-3d6609cd89b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4445496f-d189-464d-99a3-5549af3468da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 158
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c55b788d-1b5a-4405-b40c-0d85b8209174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 31,
                "y": 158
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ce02852e-13c3-4045-bf93-de28dfd35279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 74,
                "y": 197
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e9a295d2-ebbb-4911-9d79-1b99115db97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 197
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b41fc02b-83dd-422a-b3aa-a01a6b49932a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 58,
                "y": 197
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e824b256-4e06-4da8-af80-39188635d180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 43,
                "y": 197
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8fe9cfc6-6dd7-4175-ad3b-e9ca68277a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 29,
                "y": 197
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6c970232-44e5-4267-968a-5326b88294dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 15,
                "y": 197
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3f374b90-f940-4757-8f18-609c96b7db84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 197
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c40af782-1074-4d76-a6c0-2b9f44fc9617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 232,
                "y": 158
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bcde6cfd-73b1-472c-b95c-a0d55f9bcdc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 216,
                "y": 158
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2fc1a7d0-2aca-4814-8ccd-0d2c39aff7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 201,
                "y": 158
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3952db97-8c56-4152-8230-c99676dcb151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 188,
                "y": 158
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a42db396-5ae0-4766-abf2-82e10201db7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 173,
                "y": 158
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "27fb8ed1-dfb1-4f70-bfa8-91939f3d9f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 160,
                "y": 158
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "408a8243-876a-4b3a-95db-d8a949ca3a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 147,
                "y": 158
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9d5c991d-1c6e-467c-b634-5f5ab8d85149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 132,
                "y": 158
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b9966e9c-9d41-4fa3-8271-7bb177212683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 117,
                "y": 158
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a142a07d-5275-48b3-baac-9181fcf3b500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 109,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2ca4ad1c-5291-44bd-9150-7e8ab4be52e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 94,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "693a5f45-15dc-440a-8045-59541a41ac33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 78,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a75cf8e5-59bc-48a9-a9ca-ccfb70a777d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 65,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "aa5e87e3-358a-47f8-883e-826f01a8b305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 47,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "96784dbd-d75a-479a-b98e-c0a1c8370ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 199,
                "y": 80
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a4fa8371-d34b-488b-86d0-7222d699e6ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 184,
                "y": 80
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b7da83cb-008f-46de-af7d-809c76c48c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 169,
                "y": 80
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3d5cc271-4aa3-4d3d-93b0-3ec451528cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 70,
                "y": 41
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "36cc00b4-529d-46ae-bd2b-84697f58f338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 45,
                "y": 41
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5a8a8ae6-44b8-431f-ac72-790b5e6cd78e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 31,
                "y": 41
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7ca1e9ff-608d-4abe-a0bc-4eac6b846884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 41
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "23e22478-07f8-4396-8a1a-6803b8c143b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8f07311a-e543-41ba-aa2d-6189be0c8752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e4d8dae5-c5f0-44fb-a620-98c33a4dff4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "afe3078f-4ad8-4197-a1a6-69c49dea250a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f8f85f2c-0f34-4200-8b25-0bde46a21c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "aeda78d7-95e1-478f-a89d-a2ee2def2619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "40e25932-0258-4281-b74c-c186f3961b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 61,
                "y": 41
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ebd42380-3050-4edf-a793-96fbbfea71c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c7b8028c-a2e1-4e1c-85a9-2db585e58412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6d03b6ed-6b8c-4dec-b722-6557ac85a035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "08b1ab66-db0f-4860-b154-e6a102fe735a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d6c7e505-836c-47ef-9488-9f390c02b6e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "65efdbcc-183e-4042-aa66-1769cb0f7dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f0d61748-10e8-432d-9f2f-927c91dbf8d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "94b5d2bd-db1f-4d0c-a431-45be18a4f4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b330ee76-532f-44f9-9682-3d7ca61d4904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ce1cd524-c689-4cc3-82d1-4781f4b65622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a1a10be2-65ac-4c7e-baaf-771fbc06b394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c3c1ef78-875b-4162-a3fc-135bc38445a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 86,
                "y": 41
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3589eae4-7b4e-48ed-82b4-2d4262195ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 239,
                "y": 41
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "24a74161-2533-4c98-806c-705b6c52e0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 41
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "eaa567b3-ed0c-4fd6-8ddd-3f93ab4748d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 140,
                "y": 80
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "bc9cd5df-59f7-4a32-b26d-7d789f8f864a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 124,
                "y": 80
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5b1904d5-1474-4e59-aa63-9e900a70eca5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 111,
                "y": 80
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6fc1d1e3-e330-4839-90d5-295526492d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 93,
                "y": 80
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1dbaafe5-45ea-400a-a40f-76fada7b6452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 78,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "30a3b3a2-797f-4a5f-86e1-a13d0a4f4961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 63,
                "y": 80
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1e381de0-fa1a-454a-a1bc-83a5b3893fd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 48,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2d474e20-7936-4a9b-9fc1-f148b438084c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 32,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9a7e8155-f249-4b3a-9735-0f08b4db2063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 16,
                "y": 80
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "82d6ff65-d044-4f40-926d-56118a4d68d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 155,
                "y": 80
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "946409fb-0c3a-4a35-aafc-b7a528033211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7a4723b1-4148-4542-9ab6-6e370b0db386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 224,
                "y": 41
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7d5c2c62-53ef-4bc9-96f5-f43bf24f81c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 208,
                "y": 41
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "dc9d65ef-1256-49ce-95bb-6e01b146360b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 186,
                "y": 41
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b0e29faa-2a03-4169-81dc-c1e321255f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 170,
                "y": 41
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e9fc281c-16e5-43f1-853c-aacd18b46c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 153,
                "y": 41
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7b0879f4-b93e-47d7-bb5e-c873d89c0be4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 139,
                "y": 41
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0423a57f-48dd-47e3-800f-912c46a57181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 128,
                "y": 41
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a13d5c21-9900-4bf6-8dfb-03bb68611733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 121,
                "y": 41
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ea3675ef-61d9-4537-9fd2-c253c36e2532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 109,
                "y": 41
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "16d510b8-6563-423f-8a87-85a4b106829c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 89,
                "y": 197
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "58d39d7d-2f7f-4807-9503-7a23e4202d49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 37,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 104,
                "y": 197
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6b6916f0-e4bc-47d3-bfae-69dc2e30e579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 38
        },
        {
            "id": "fe8376fb-5e26-44a2-bf0c-378e0c73c13f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 52
        },
        {
            "id": "456ec32a-93e1-4197-83f8-c32e425769dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 65
        },
        {
            "id": "a7273d31-5fd8-4142-a1b8-4698614b50c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 74
        },
        {
            "id": "baec3abc-6458-4b87-99d2-74c397a2b8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 97
        },
        {
            "id": "7879b3a8-10ae-42ed-b0cf-fd4e5d5acee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 106
        },
        {
            "id": "8e7ff646-610a-4bf5-a420-64a8b66c5f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 32
        },
        {
            "id": "2636a5be-9bb2-4502-ad99-fc40718054ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 84
        },
        {
            "id": "24c2d255-b38b-44b8-b080-bd47fb36ee9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 86
        },
        {
            "id": "f344c0fe-0904-40c5-ab48-1d480036864f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 87
        },
        {
            "id": "e770ac4c-77e3-43fe-b3a8-69da922e284b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 89
        },
        {
            "id": "aa51a403-8423-4eaf-a08a-4993f26358f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 118
        },
        {
            "id": "28459c6c-359e-4546-9122-dcbdb6612283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 119
        },
        {
            "id": "8076a10b-b0f8-4710-84a6-c992708ebf43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 121
        },
        {
            "id": "45f2ebd7-3342-45a9-88c3-b8dbcd74828e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 52
        },
        {
            "id": "0b26291d-63e9-45ee-a4a6-4a36d8335a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 65
        },
        {
            "id": "3059f7c3-4b2f-415f-a894-58eaa2aa3c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 74
        },
        {
            "id": "0196e659-f1f6-4799-9e86-bcf3f503d347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 97
        },
        {
            "id": "c574c3a8-d67c-4581-abf0-f844131087b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 106
        },
        {
            "id": "d0a7f8fe-713b-44b7-bf3e-ddfa29c8d5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 34
        },
        {
            "id": "8efc1a1d-1904-498d-809e-65b5d57b81e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 39
        },
        {
            "id": "c095cb08-5cce-442a-9c15-2f6ba5794091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 52
        },
        {
            "id": "77ff34cb-d198-495a-9b50-dde64ccd0bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 53
        },
        {
            "id": "b4bd3fc7-1a0a-41c3-b783-5c2c1ad667e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 57
        },
        {
            "id": "cc0100ed-82af-4992-b03e-df63f96e7c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 83
        },
        {
            "id": "2da2ad67-5b38-48aa-aeb2-d363db3a6f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 84
        },
        {
            "id": "b6ccc444-2759-4858-9f75-4b6556001747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 86
        },
        {
            "id": "16dc8f3f-1feb-42c8-b6cd-36ff061e796e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "1c2e51e7-fdcb-47c1-a59e-19f0691c7b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 89
        },
        {
            "id": "03a02df2-df09-44d1-be4f-0de0bcda4d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 115
        },
        {
            "id": "a5a79ada-5016-4341-9fdf-abc93fb68d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 116
        },
        {
            "id": "59d3025e-982b-4084-af18-fc9fe01ba865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 118
        },
        {
            "id": "b46c18af-4957-4769-978d-f619a80f90f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 119
        },
        {
            "id": "a910983d-910c-4ced-8454-265d67c20115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 121
        },
        {
            "id": "2b5504e4-04e6-4576-8f55-e5c701c9799a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 49
        },
        {
            "id": "4c8b8d80-9461-4fcc-a50a-c4dc49f2be26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "cf28d785-b1bd-4225-b558-7d48131b4124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 55
        },
        {
            "id": "5edf9502-4754-4231-b6c7-72b322c27a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 65
        },
        {
            "id": "4817f7b2-fca6-4ce3-af9f-67aaa97f9b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "c1491cbc-f3b3-44a6-a4f6-3c1a231f3524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 86
        },
        {
            "id": "58bcc409-920c-4e15-8a6e-f464dc24e1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 87
        },
        {
            "id": "4e96212d-9a3d-4d55-a84a-50484688adfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 88
        },
        {
            "id": "67272d35-7d0c-48de-a2ea-4999b46812c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 89
        },
        {
            "id": "591de2ae-c78f-4447-a9ef-ff8ef96c84c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 90
        },
        {
            "id": "8af2bdc1-3873-4fb3-96cb-1cf6c06636fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 97
        },
        {
            "id": "21c87128-eba0-44e0-b856-d7a5d0d75d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 116
        },
        {
            "id": "88f22c7d-81c6-4d2d-a734-18c665ed9abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 118
        },
        {
            "id": "eb8590cf-901b-455e-92f5-74252347d3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 119
        },
        {
            "id": "7e2bfb8c-4897-43ac-b8aa-fa7995821d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 120
        },
        {
            "id": "e6167cb5-fb45-4631-950f-86fdd34e5cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 121
        },
        {
            "id": "64e55c6b-c09c-422b-9f58-dfc8456eea5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 122
        },
        {
            "id": "3cd16a0c-6a67-45e2-9fa5-c84f25addda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 34
        },
        {
            "id": "40f87ff5-bac2-4579-9e6f-cdee656fca14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 39
        },
        {
            "id": "31a03c16-14a4-468a-a150-d9e6f68005a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 52
        },
        {
            "id": "da9895b2-62fd-4ef9-a08b-2b01ba0be347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 53
        },
        {
            "id": "dd4e87ef-8704-476e-bfae-f0a17efc967e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 57
        },
        {
            "id": "6954bd83-0635-482b-989e-dd66871061a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 83
        },
        {
            "id": "e018d3b6-b43a-4c25-857c-c6713a577d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 84
        },
        {
            "id": "c160c3f7-8497-4d54-a777-0b7bf5899410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 86
        },
        {
            "id": "b6d039b4-c796-46a4-ae4e-e3ddfa213ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "e73a18f4-ae5b-472b-8ea2-056a5155c318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 89
        },
        {
            "id": "9a619fa2-51ce-4388-9576-ef0314c77552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 115
        },
        {
            "id": "d4832030-65e3-4ef6-9ca9-6b5cde856f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 116
        },
        {
            "id": "7836c05d-4107-4abd-9c54-ab0bbeccb5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 118
        },
        {
            "id": "c952495c-8d83-49c7-b1fd-a996abb284f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 119
        },
        {
            "id": "de5b3895-85bb-43af-aff5-1bf7591d7cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 121
        },
        {
            "id": "b8dcc772-3d1e-49f3-bb42-a48788d19d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 34
        },
        {
            "id": "9af7c5fb-fb83-4cf2-bd12-e810fc36112f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 39
        },
        {
            "id": "d8638b07-0803-4ab9-be0a-3b599747ea14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 45
        },
        {
            "id": "ae79d1be-eb32-4d1a-914c-0bf407c5d472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 57
        },
        {
            "id": "c88b7599-0655-4ebd-a96c-97a4b8d60b0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "e2cc114f-451d-4cf1-a3ea-86fc5457840c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 86
        },
        {
            "id": "9aa8b71f-ffad-4c84-8e4a-7d55f5946940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "73df2dce-e0c1-4ce0-9f42-9577bf5def54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 89
        },
        {
            "id": "b9998e43-2b1f-47f1-b6a5-2df6c9e0914e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 116
        },
        {
            "id": "b12d8298-dfe7-4b5d-baab-f6156bc74c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 118
        },
        {
            "id": "329e0669-1735-4aaa-b51a-d06399ef332f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "a56e1aae-2e01-4c01-9817-69b7af20a5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 121
        },
        {
            "id": "b925f08b-b6a3-4cf3-bfc0-28749ee97ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 38
        },
        {
            "id": "812ff83a-24eb-47a2-ae8e-722ad9d04b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 44
        },
        {
            "id": "c7282a44-dbc2-461d-b244-b14154f72d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 45
        },
        {
            "id": "bc461b8a-b573-44d0-a67c-1f017ab42855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 46
        },
        {
            "id": "53fe4a76-3350-468c-b5a5-693c89ac8818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 50
        },
        {
            "id": "a6ef29e9-68f3-44df-bd14-d80e02cfc17f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 52
        },
        {
            "id": "d6c6e089-1a5e-491c-96c6-2ae005a3387d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 65
        },
        {
            "id": "75837943-18ba-463c-b188-83e092df92a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 97
        },
        {
            "id": "167c1575-a2e6-4634-be0c-cdc303609a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "97f5fc18-9c6e-466c-8cd5-c7c7eb62a496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "53a8ede5-0eba-421f-bc80-55b8fe4ec4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 45
        },
        {
            "id": "567ac021-da0e-45f7-bd4d-c57c66c9c688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 55
        },
        {
            "id": "b13bbb24-aabd-433e-b8fc-4683449980d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 83
        },
        {
            "id": "b54ac412-563b-4ffc-9a51-40b9fdf778cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "e355d9a4-88b0-4452-bab3-9590e1ca6d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 86
        },
        {
            "id": "c34e71cf-eab0-4fe2-9c25-3dbda977f416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 89
        },
        {
            "id": "5859992d-cd74-4e8f-afdf-3e6291fc05a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 115
        },
        {
            "id": "abc84560-a4b4-4c48-b320-7e1a262a5ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 116
        },
        {
            "id": "1a2f009e-df5b-47ce-beb6-64de76fe527d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 118
        },
        {
            "id": "c16b6a13-d57b-4f82-8ba4-c738660089b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 121
        },
        {
            "id": "04125825-5320-4281-aa1a-c9d350e9b277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "b6fa0006-1966-434a-b43d-e394b0182118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "54dba47e-753a-4ad1-bd82-8e99064c8c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "f4b4ad35-61bb-4ba5-96cc-30b6c32d065e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 86
        },
        {
            "id": "4c97db93-fa1d-47f6-9b60-2499f5f8d0dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 87
        },
        {
            "id": "cec28904-7db9-4bb1-a84d-13833f2b31b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 89
        },
        {
            "id": "2f926380-7e6f-44d5-9fd9-5b0869142bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 116
        },
        {
            "id": "0b116b90-dd76-4d21-81fa-553a4cfb96d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 118
        },
        {
            "id": "31743db7-9b2b-401e-b260-79ab1f28cbb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "7b42dfa3-78e1-4a4a-bdec-d5fc21950b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 121
        },
        {
            "id": "d8e23a0f-3dfe-409f-a9bb-5441b5b7415c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 34
        },
        {
            "id": "c8f022b1-ceb1-4fa9-9f7c-f743711fbb6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 39
        },
        {
            "id": "fbee0424-572b-44aa-9669-d6972efdb245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 55
        },
        {
            "id": "bc880f94-3e68-4b5a-9c68-f3c3f4e528a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 84
        },
        {
            "id": "46aed632-3b16-4614-86e5-6ca94661237c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 86
        },
        {
            "id": "05e92e9a-265f-4a75-9291-e09d4a63b2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 87
        },
        {
            "id": "8b2fce0f-d105-42ab-9d3a-82abbe48aa29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "8ffe7c9f-fa79-4ad5-8034-c64f93aced54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 116
        },
        {
            "id": "9ce73f6a-15e6-4096-94ba-6aeb69aa62e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 118
        },
        {
            "id": "fa214090-516f-47ce-bfd4-8add103a06e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "c3eac34c-5855-4fa6-84a1-4ca6de8831b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 121
        },
        {
            "id": "484bde98-4833-433f-8460-a67d658fd02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 38
        },
        {
            "id": "a4d63a05-ed76-47c3-9b29-bd206c97fc66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 44
        },
        {
            "id": "1b6b1bef-5223-40f5-ad23-289c23464810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 45
        },
        {
            "id": "f7d247cf-cc72-4ca6-a2bd-460fc5298278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 46
        },
        {
            "id": "9dc57824-b926-4cca-bc1d-0973d68c4df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 52
        },
        {
            "id": "510a8e39-bf2c-4b05-a742-c831a1652b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 56
        },
        {
            "id": "a95c9a2f-5953-46bd-aa5a-35f3a9ab162f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 65
        },
        {
            "id": "bdb68667-c956-4c18-a45b-37a8a61197fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 74
        },
        {
            "id": "72f4361e-9f70-4b29-9012-5fafd56d8f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 87
        },
        {
            "id": "16cbe8fa-af1a-480c-9e61-e285ee335cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 95
        },
        {
            "id": "2fe4f6e6-2fb8-4852-a118-51dd8ccd4842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 97
        },
        {
            "id": "714781b0-40ea-4672-a78c-0e0f44bbeb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 106
        },
        {
            "id": "6304d493-09e2-4900-be9d-1017215c84a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 119
        },
        {
            "id": "46459efe-34a3-47ba-b496-6bc76c0d3649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 65
        },
        {
            "id": "d5972c5e-b470-4d2c-8813-6a185ae30233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 97
        },
        {
            "id": "a2cccfc1-def6-44c0-a57f-048950dd2d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 34
        },
        {
            "id": "62c20ca6-5437-4bf0-915a-573fd687824f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 39
        },
        {
            "id": "4557af1b-030f-4dba-89e6-b0921416a1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 45
        },
        {
            "id": "a7b3682b-fafe-40da-b2d6-77266d4c169f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 53
        },
        {
            "id": "bef11aad-bcf3-425e-9a50-76903fc2f6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 57
        },
        {
            "id": "2ccd8021-2ed6-42ec-9da7-d0070c509502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "6cfff0f1-e63d-42ef-855d-260fbcbf4617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "4236df23-d9fa-4f7a-99a2-1c1042e0e7a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "b52efb34-7e99-4a49-8ca3-e002821f0241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "1e804979-3b05-4ab8-9860-c6d0d5edad7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 88
        },
        {
            "id": "646dc830-7293-4908-a090-2ba9eb015f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "abeedd19-590d-4250-b2b2-4def3ef51911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 90
        },
        {
            "id": "a0dcee0a-a392-476f-b4fc-878ddd4496ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 115
        },
        {
            "id": "ff9c1974-9a15-421e-baa0-c2516f785149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 116
        },
        {
            "id": "40614064-5599-4ec2-827e-9118483718f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "f7c6fa26-125b-43e9-b8b5-08d5ba010396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 119
        },
        {
            "id": "bb880911-709d-4eff-bcdf-4cb93ee396a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 120
        },
        {
            "id": "b0c16411-51d9-47c4-af88-bafba1fb4f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "0e89c123-64b5-4f19-91c8-7cd2d2c66e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 122
        },
        {
            "id": "17303ae6-fb68-4847-835e-e3a3da65cf4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 95
        },
        {
            "id": "724aa3f6-0626-45e2-b709-2977b908487d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 45
        },
        {
            "id": "561fd039-7c5c-4245-a52f-6cb57c04faec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 95
        },
        {
            "id": "b2593bc4-1051-45bd-8bf4-5c4b3bd278aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "51bdaeb7-d115-471b-b80c-03e212e1593c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "9bcac218-1954-4518-830d-1b2b660f45ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 38
        },
        {
            "id": "ae0c023d-15f7-4ad2-be5a-0e4075c28323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "154c6c81-253e-4495-b1b8-e3a32941e98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "d90ff501-e573-4c5e-96ce-f6296567a028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "68b96569-211d-4fb3-a747-7728b3390fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "c51ce446-c117-4097-a061-f850112453de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "8a5ba517-0901-48b6-980f-2524422bf618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "c0081ab2-0dbd-4098-98e5-123c653a283b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 95
        },
        {
            "id": "4ba5d2ee-d4a5-4c0b-9b76-b3527c4e43d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "2fc49445-e788-4a4b-ba39-66ce0a19cbc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 106
        },
        {
            "id": "307988e6-eeba-4aef-94d5-4de25ca26c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 34
        },
        {
            "id": "e59be680-ff86-411a-8add-30af7d3378d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 39
        },
        {
            "id": "4c755c4f-e019-4e5c-8534-70b98b427f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "8b797862-cebe-4aa5-af52-1d5b08c20f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "e6cfe28c-6d65-4428-b83e-6dd2d44ec105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "40d26e1f-1997-445a-b5ee-67b01a33c682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "d055972b-daf4-402e-bb2b-3a36b7abbf3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 116
        },
        {
            "id": "e4d963ef-3061-4b5d-9799-98aa413e5eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "f9d5af93-a0e5-4082-b032-3a96a2a74b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "aafbeb0f-e85e-4cb6-8f4e-14b5c28604a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "864c454a-fdab-4e0b-9456-8d5cb37aa706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 95
        },
        {
            "id": "9b53e63c-99e0-4f9d-870f-1dc1a1710671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 45
        },
        {
            "id": "93bc7984-cd72-42f5-9f4e-39d953d1b87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 52
        },
        {
            "id": "027ea1a6-d6df-47a3-b877-5ecb11e9298d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 65
        },
        {
            "id": "dec1963a-96ba-4383-b428-78a948e267d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 97
        },
        {
            "id": "4de33450-3335-4f5b-bb5a-ca49ee112239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "fad972f2-5e33-41d6-8026-69b47385d6be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "15dbc249-6fb1-4f9a-b1fc-a277786e2f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 45
        },
        {
            "id": "a540d3d1-04b4-4919-9649-b11d69bd393c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 53
        },
        {
            "id": "f88c5810-8bcc-43a6-8cc2-3e22c5c8f89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "40243ce7-2ed3-4932-a410-7468977b899b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 83
        },
        {
            "id": "d27441ce-352b-472f-bf02-195df8bcea27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "cfdf8ad6-3bd7-471d-8505-15935195b081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "6b948508-13c8-4868-89b3-dda46f81434f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "0e1d9edd-7509-4302-b147-4edc6e32156b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "7defdc8c-f03d-4341-8513-7cd257c5bb52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 115
        },
        {
            "id": "ff68370c-af1b-4454-9b7f-d8840b684167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 116
        },
        {
            "id": "52c20699-8e5c-4705-a087-c35b1a6c45cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 118
        },
        {
            "id": "6799de24-ad4f-4e55-babf-064b9fc33d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 119
        },
        {
            "id": "01802324-37f2-4520-9885-c97eae39554b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 121
        },
        {
            "id": "28c733b5-8829-49fd-807f-fbe597698238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 95
        },
        {
            "id": "cb8f28b3-fdcb-4e79-bbde-da18f66f0ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "83d46b49-15c3-43c3-8548-780d474a859a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "a9cd1804-a12c-4e10-8f70-9bf46c3c3be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "4d2624f9-f54b-46f7-a500-ef3192ebe75a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 95
        },
        {
            "id": "452bff90-c838-4373-8153-8ed86d7b2ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "8e5f7cbb-9e8d-456b-8cd0-b269d297d903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 95
        },
        {
            "id": "c66df1c0-c86e-41f1-8ed5-d4e14e5c4502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 95
        },
        {
            "id": "0cff49a1-423e-4529-b700-6095f2524c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 38
        },
        {
            "id": "ad03b10e-17cb-46f4-b3f8-2cc366b558fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "759dfeb7-f2d4-4542-98ec-879e7a644b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "480e0598-81d3-43eb-b850-d54285290e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "cecb8caa-498f-4e9d-acc9-8db3b1e339e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 52
        },
        {
            "id": "9ec51124-7e20-494e-ae42-5f174a4c668b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "1fecc584-e909-4c37-a6f9-8cf78e372b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "77ad03b6-ba5e-4022-9c3b-d6b9bc4d5c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 95
        },
        {
            "id": "fdbda0de-b9c3-4805-8e90-5813f4054ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "653031e9-fe75-40ce-b816-2cd15c721960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "32645f10-7055-4baa-970d-184328d49f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 95
        },
        {
            "id": "38334569-c83d-42c4-a86c-1d41adc71fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 38
        },
        {
            "id": "d4997bff-552c-47fb-9ed5-9998414fb366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "799c84db-8a0f-4d8c-a238-414fa00b93f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "4adb4f83-258b-4c7b-901c-ca6f06382014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "84ec368c-4cfb-4dff-a36a-d4d3c8203354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 52
        },
        {
            "id": "df71ca6c-4027-4f30-a574-db8388047e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "22fd8d8e-34c1-44a5-806f-74d1ff0e6ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "e2122d80-2f58-403d-8c44-8a9fcc2f1839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 95
        },
        {
            "id": "4c560d08-f996-45c4-80bc-fb261831d079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "be9a7b9f-0205-4623-b059-ab72ef7ae467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 106
        },
        {
            "id": "4d8723f4-f9c3-4536-a745-939c627b8457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 38
        },
        {
            "id": "07dad2a0-f55f-4efa-af94-a5b6284c20b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "75090265-55d4-46f5-9a80-9ddc13c780fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "eb1495d4-0004-4b29-857f-dc92209e0a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "44866cd9-d8c7-4fc9-b570-c8c3289896c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 52
        },
        {
            "id": "db56f0b3-b3fe-4429-9735-a7dfbe66cb48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "cc580cfa-9e3f-4d1f-a854-29a74799e96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "c18b9074-30c6-47de-83b1-fb2fbfc1df9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 95
        },
        {
            "id": "d1eed892-a3e7-4202-ae8d-8a519881c03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "2174272a-f8d5-489b-b603-f5774aa04937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "a53128c9-90ef-46c6-ad2a-b319f1f7f27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 45
        },
        {
            "id": "357905b5-4eaa-4321-b097-5c30a60fedbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "56872126-ec7a-43c5-95ce-b222a3b2d7ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 65
        },
        {
            "id": "1ce973d1-ceb3-4f90-bc14-70a4eb985732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 97
        },
        {
            "id": "f8c15ad0-1e69-4376-9e60-204ac531f44c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 38
        },
        {
            "id": "508580cd-8041-4116-adad-f102857136c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "acb5a643-8a4a-46fa-973e-2ec4cfe65e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "f6da28db-2a13-430e-8111-8dcab3ad1464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "fa97fa97-c99e-4dde-8aeb-808b81f9a2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 52
        },
        {
            "id": "c4383083-c154-4242-8cd9-71d979ddffac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "d5284a65-f041-4634-a3ac-7c3ccc1f376c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "129e093d-eabc-4a6d-aa79-dc97741ad8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 95
        },
        {
            "id": "ac81b434-b86d-4b56-8026-b4546b778fb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "a27ee3a6-24db-4bda-9eba-802a9c141346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "e08f17a2-942f-412f-9bbf-4cce978b3afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 38
        },
        {
            "id": "5a7a6e22-9d35-4a1e-b3b3-a1ff4bf2b79f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 45
        },
        {
            "id": "cb84efc5-5216-4918-998a-a04a2c7ff737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 52
        },
        {
            "id": "cf717d94-9998-484e-8a7a-4e99a24c057e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 95
        },
        {
            "id": "10ae1221-7881-41c0-bd3a-564dde197b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 48
        },
        {
            "id": "38670f91-c8ac-4768-b47b-0b364166323f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 95,
            "second": 52
        },
        {
            "id": "ee0cbcc9-685d-478d-bfcf-775a15f7c2be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 53
        },
        {
            "id": "da55ce3a-7db9-40a9-8bfa-90b39536ac29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 54
        },
        {
            "id": "8da29e24-cfd3-4808-bb29-a6ef1897e222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 56
        },
        {
            "id": "3b161c57-f286-4fcf-8d4d-a1393202f38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 57
        },
        {
            "id": "252ea256-d942-4818-86e7-4438dcf6e059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 67
        },
        {
            "id": "9c6a815c-c171-475b-8a44-e65271e0b6c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 71
        },
        {
            "id": "5b7893ff-41c3-49ee-a45d-9d2643918d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 74
        },
        {
            "id": "d499ff36-caeb-4cab-8996-b069f1f0c05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 79
        },
        {
            "id": "f6d24ab9-baef-4192-aafc-5e43adbf27d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 81
        },
        {
            "id": "18a48c61-a77d-4b65-b9bf-ff06c37285d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 83
        },
        {
            "id": "9e49cc51-2c2e-4321-8415-5bddab8da5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 84
        },
        {
            "id": "5672cdf6-d632-4e5e-95d8-1894c9617563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 85
        },
        {
            "id": "c542a63c-d1f5-4dce-9bd9-cdcfdef7c014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 95,
            "second": 86
        },
        {
            "id": "104e9cf8-fd2f-43a2-834c-b6d867919f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 95,
            "second": 87
        },
        {
            "id": "6b670b25-80b8-4015-8628-064b910bb1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 95,
            "second": 89
        },
        {
            "id": "0bfd04eb-bac8-48fd-9f48-e9aa2d2b96eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 99
        },
        {
            "id": "5af2d0ff-afd8-426a-9b3d-977ef750c125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 103
        },
        {
            "id": "87de33de-68a2-4e19-af0f-a4e5ee5b5a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 106
        },
        {
            "id": "373106e5-97af-4af1-8d03-b7fbdc8bd01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 111
        },
        {
            "id": "3517743b-9a5b-4fff-92cd-750c5df4c49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 113
        },
        {
            "id": "7490d546-37fa-487c-9174-db3137a3c6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 115
        },
        {
            "id": "7d412b57-6cec-42ad-a0dc-7fd0a7df9cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 116
        },
        {
            "id": "3bfdd21d-64f8-4ff3-9c68-37e31eecb92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 117
        },
        {
            "id": "05c3e74d-8508-44b9-97ae-e89c2649c330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 95,
            "second": 118
        },
        {
            "id": "c111be03-165d-4f0c-b360-a4bdf32699a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 95,
            "second": 119
        },
        {
            "id": "5a9283ab-026b-4509-ae1c-ef247f8769ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 95,
            "second": 121
        },
        {
            "id": "711c4036-6f88-4831-b7e8-350ceae363d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 34
        },
        {
            "id": "e5e88dde-049a-4dc1-bd08-2bcc436496a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 39
        },
        {
            "id": "748ad671-78cb-457b-899a-c763569280ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 45
        },
        {
            "id": "6b2ce5cb-b240-4a00-b5b4-a002bc187984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "5dbaa9ab-0f00-4c9e-a8bb-f81c1f1570f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 57
        },
        {
            "id": "bae2d2fa-c116-4cf1-a7f7-b851565dcf4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 83
        },
        {
            "id": "6cd70c26-6bdc-4e7b-a0d7-e0448fa90d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 84
        },
        {
            "id": "4680a726-5608-4b04-801a-ecd9bbdb208d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 86
        },
        {
            "id": "2abdcee4-4dcd-46cb-9e96-a65397a3b35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 87
        },
        {
            "id": "f20c663c-4cae-46a9-8a3b-2d601bf6cf11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 88
        },
        {
            "id": "7321f4eb-d1f6-4d32-8ffc-f64f350f4dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 89
        },
        {
            "id": "a062d7ab-d7db-49af-a589-a68e6e733b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 90
        },
        {
            "id": "90777925-be1e-44db-82d1-ad34188ea6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "509d0d7a-a5d0-420f-9189-1ecfda38321d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 116
        },
        {
            "id": "ae5df70f-b621-4da4-a52c-1c8190594cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 118
        },
        {
            "id": "c95af127-2a71-4f87-8473-2bb3b84b708e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 119
        },
        {
            "id": "5bcb10c3-01ae-4ed4-b626-cee0ab87ebc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 120
        },
        {
            "id": "604efbec-2be9-4b13-8c5c-7041065de677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 121
        },
        {
            "id": "787b1647-fda4-4f9f-a5b0-74dfe56e100d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 122
        },
        {
            "id": "4f911460-8e5c-457c-9f3b-77416831d77f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 95
        },
        {
            "id": "1785dbdc-f4a7-43a9-880a-47ca3a9a96dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 45
        },
        {
            "id": "1dfeaf80-473c-449a-b218-46a1d79a41da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 95
        },
        {
            "id": "01ef03d2-e6a2-4819-93cd-b02168229a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 45
        },
        {
            "id": "eb995716-b1fe-4716-ba96-04f8db2b25f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 52
        },
        {
            "id": "5a258e72-ad13-4b92-86b3-735864c627bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 38
        },
        {
            "id": "66b79e11-69e3-4aba-b005-7cb9826ae84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 44
        },
        {
            "id": "74a92d22-404b-4b02-a55d-b2c90ea9b311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "ab52113a-102e-4efd-a8f8-740215d49e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 46
        },
        {
            "id": "2dd8be99-073f-4606-afd2-72f60804bd1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "43075819-072a-4727-9e82-a1757c88b702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 65
        },
        {
            "id": "7ba5d179-95a8-465f-b943-3ac8047f9065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 74
        },
        {
            "id": "57f5410d-1ce1-429b-90d4-c706062fbd4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 95
        },
        {
            "id": "5ae25711-eb54-4301-8725-b08305b7c941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 97
        },
        {
            "id": "ce73d049-f1c2-48b1-bc60-a96b8118d307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 106
        },
        {
            "id": "a1c58510-cbb0-452b-af43-f62db355b0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 34
        },
        {
            "id": "ea8a2203-e368-4361-9139-89a6d8b67e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 39
        },
        {
            "id": "7cee7a05-821f-485a-8f9c-c7b0e9b2a9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 84
        },
        {
            "id": "07a7c5f2-2bd6-43d7-a388-c9bf680649dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 86
        },
        {
            "id": "38610798-b09e-48f3-b0fa-fe4c24dd1d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 87
        },
        {
            "id": "412f5b03-b866-4445-be14-229bdefc9def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 89
        },
        {
            "id": "985ebabc-965f-45e0-a2bc-a15aae380f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 116
        },
        {
            "id": "453aa0d1-f994-487e-bb6f-cea98a5cc78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 118
        },
        {
            "id": "fc3a063c-9b97-4c6a-ad84-475399c43385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 119
        },
        {
            "id": "37915faf-1411-4aea-8c57-ef137c987395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 121
        },
        {
            "id": "f5d3b66e-cdfb-4999-bc8f-b41b95996c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 95
        },
        {
            "id": "f510de9a-4d09-4998-b430-d76c2ec16630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 45
        },
        {
            "id": "481c7e01-5e03-4826-bfc3-49ddde03336f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "357b3284-2a48-46dd-aae8-87228cd69820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 65
        },
        {
            "id": "24329fcb-9d3f-446e-a8cc-4f46dd8d23ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 97
        },
        {
            "id": "54e49553-de2a-4cb7-9012-e9083c6926fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 34
        },
        {
            "id": "52889d10-497e-44a7-a4e6-997d5c4b4979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 39
        },
        {
            "id": "fe426143-5ba7-443b-b7af-400d63413213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 45
        },
        {
            "id": "df1986f4-7e77-4086-8772-3cbf2a0606ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 53
        },
        {
            "id": "adb23837-3626-4f3c-b748-12aab4d257ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 57
        },
        {
            "id": "01dcc279-2743-4d4b-bfbc-5484e99bb9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 83
        },
        {
            "id": "1e9b3339-c7bb-499c-80b0-66d0b6759244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 84
        },
        {
            "id": "f57303c3-3ac1-4610-a7f3-616677963670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 86
        },
        {
            "id": "6a93e369-9a1b-441d-bdca-482f8a4c6bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 87
        },
        {
            "id": "3574267c-15ed-48ba-be81-f3e1b2e25747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 89
        },
        {
            "id": "7cbb7ca3-b038-4298-a907-c300e91beced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "9b6073fe-94f6-444b-a836-9dcf4d015b31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 116
        },
        {
            "id": "7da52d95-9d3d-4be9-b061-1db182f52969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 118
        },
        {
            "id": "c8ac0fcf-497c-42dd-950d-622485a11e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 119
        },
        {
            "id": "13a93fa1-2527-48fa-a671-6f1c11ca8f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 121
        },
        {
            "id": "84ff750a-9931-497e-afb3-3ff7e95fabb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 95
        },
        {
            "id": "b738e182-24fa-4f14-a6a6-7569ec816a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 44
        },
        {
            "id": "dbdc9b08-47b8-4393-a7dc-5d47e9e4b6dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 46
        },
        {
            "id": "a7b9db57-b987-451e-9751-f7d86db13b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 65
        },
        {
            "id": "4d5f84e1-5cbb-4244-8b93-60206aa26d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 95
        },
        {
            "id": "87a91dcd-eea1-4878-81ed-56e0242e1ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 97
        },
        {
            "id": "770aa126-466b-411f-98b4-a47874ba2154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 95
        },
        {
            "id": "6d6e2690-3f11-4256-9707-fdf0cdc00ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 38
        },
        {
            "id": "03b180d4-1d00-4faa-81f4-f5dc87b06af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 95
        },
        {
            "id": "409e8c4d-24dd-4f16-bad2-ecc752da0560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 38
        },
        {
            "id": "5c93bd29-1207-44d7-854a-8c004228647b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 44
        },
        {
            "id": "ede2aa87-d5bc-478e-a24f-732944e504f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 45
        },
        {
            "id": "c732ca13-7eb5-4ada-bd79-e0042ac78702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 46
        },
        {
            "id": "77813af0-b3f4-4978-bcdc-48716e8f46d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 52
        },
        {
            "id": "0eb4d7e9-7140-4678-a101-d6681a6a14e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 65
        },
        {
            "id": "5d884feb-2421-4e83-b8c6-c0317cdcc665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 74
        },
        {
            "id": "088998b5-26f2-4cc7-a0af-320015eb58e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 95
        },
        {
            "id": "75f6cbc2-2c5c-454e-9357-937f08e12380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 97
        },
        {
            "id": "44b61b15-456e-49fa-91b4-8ff739ec42fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "0cff9e6c-3632-4437-b3c0-7f69551cb82a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 95
        },
        {
            "id": "2f0758cf-7888-46c3-9202-f078a7958955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 38
        },
        {
            "id": "ff99544f-1578-426e-9ad6-2ff4b9ea4680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "c331b02d-09a5-440f-bdf4-4d17c284dec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 45
        },
        {
            "id": "412fd0a0-dcba-466c-8f3e-49f04aec7577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "627f0ed0-f2ca-4d56-9b6b-e5846b09b0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 52
        },
        {
            "id": "d7b52dcd-b369-4739-8e55-0092dd81a85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 65
        },
        {
            "id": "0ed67eeb-acbb-44b0-9ffc-d6830b833181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 74
        },
        {
            "id": "d68ec723-2325-4f75-98d5-d4cabd0cd49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 95
        },
        {
            "id": "ff7280c0-7f2d-45d3-8b36-20c05257828d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 97
        },
        {
            "id": "77f6c596-ef46-40ea-9986-f99abfd5c5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 15,
            "first": 118,
            "second": 99
        },
        {
            "id": "600cb06b-fb70-4b0d-b8d0-0b9eff3512ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 106
        },
        {
            "id": "da0f48d3-1bfc-4466-a926-739bd8c677b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 38
        },
        {
            "id": "a5fbde05-c99d-470d-898d-769bd1f64763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "f170155d-55bb-4152-bfcc-8998d5949feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 45
        },
        {
            "id": "a0fa98e6-654e-4d34-927c-0dbc703e1c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "81b11f95-69d3-4f92-af7d-6e9cb51aa428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "71503828-c1a8-45de-9aeb-5e3b535e32e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 65
        },
        {
            "id": "8e716140-a7d4-479a-839e-c0f3c8347e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 74
        },
        {
            "id": "b5ce2684-2734-4179-a2d0-24b256596974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 95
        },
        {
            "id": "921aaf67-e2a7-4359-a152-5dead29f9f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 97
        },
        {
            "id": "5c9d6b48-0f56-45b5-9eeb-cdce3ab7bc94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "fada0eeb-2c0a-4c94-a0ec-6dce0460874f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 45
        },
        {
            "id": "b8df291a-f00b-4ae6-a2b6-40d70c725c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "951fde8f-ebb5-49b3-90e5-9ccf709f87f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 65
        },
        {
            "id": "0fba0379-4e09-4187-a61e-a985f62fa824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 97
        },
        {
            "id": "9ac4c9da-fe0e-4e43-8a56-3637878d9814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 38
        },
        {
            "id": "a2fca2d3-1d77-467d-a966-4e4d9f6a9a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "b9fefc44-80ce-4cea-9790-eb486c161e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 45
        },
        {
            "id": "31120f8a-f6f6-41ef-8a7a-d046d1758d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "3f8d149b-f886-4cec-bb33-9e502d4dee62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 52
        },
        {
            "id": "645b201b-89c6-43ab-b339-49dd05172877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 65
        },
        {
            "id": "18486525-bbb0-487f-8529-9ccf4245fb87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 74
        },
        {
            "id": "c1e3fa2a-8d9e-4f31-862b-7743fb0472c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 95
        },
        {
            "id": "5d05eff4-5d90-4aa8-9f1c-ad85819d18f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 97
        },
        {
            "id": "a06a7c27-6105-4447-96d8-493bf0f3e3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "ad3810df-6ff7-4c44-900d-e7287fee369e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 38
        },
        {
            "id": "41fcfa15-6010-41c9-a64a-e55c2f38fcc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 122,
            "second": 45
        },
        {
            "id": "0845dc90-ae82-496c-a1c4-5265ee301789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 52
        },
        {
            "id": "0b85b782-dcc8-4886-a031-07acc575b19e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 95
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}