{
    "id": "4d4c3795-3e52-45d4-be6b-2952954108c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": -1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2459fac2-3102-4d98-92f3-4a7233247ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d4c3795-3e52-45d4-be6b-2952954108c2",
            "compositeImage": {
                "id": "ad01917b-e8b6-4011-ae3b-2cccbf8a052e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2459fac2-3102-4d98-92f3-4a7233247ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96ce59c-4090-4020-af5e-ce1c6aa54d8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2459fac2-3102-4d98-92f3-4a7233247ac5",
                    "LayerId": "29ffd359-b303-44e5-8087-a1b8d5f07063"
                }
            ]
        },
        {
            "id": "658b4c59-7dec-4e92-8570-ae3fec2ff000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d4c3795-3e52-45d4-be6b-2952954108c2",
            "compositeImage": {
                "id": "74bf32fd-453f-4c29-97ba-70b774d96085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658b4c59-7dec-4e92-8570-ae3fec2ff000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d5ac94-c01c-4724-9f3c-5357c03f1e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658b4c59-7dec-4e92-8570-ae3fec2ff000",
                    "LayerId": "29ffd359-b303-44e5-8087-a1b8d5f07063"
                }
            ]
        },
        {
            "id": "b2c9c96b-6004-4db2-9c6b-7aa576f836dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d4c3795-3e52-45d4-be6b-2952954108c2",
            "compositeImage": {
                "id": "b6cdf9eb-7fc0-4ce4-9dd8-b7c58aeb40ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2c9c96b-6004-4db2-9c6b-7aa576f836dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5855ba-e3d1-4539-abd8-0de13108f689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2c9c96b-6004-4db2-9c6b-7aa576f836dd",
                    "LayerId": "29ffd359-b303-44e5-8087-a1b8d5f07063"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 128,
    "layers": [
        {
            "id": "29ffd359-b303-44e5-8087-a1b8d5f07063",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d4c3795-3e52-45d4-be6b-2952954108c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 64
}