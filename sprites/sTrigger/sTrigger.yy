{
    "id": "1fb90003-7e68-48b9-b4e5-fe0190e71518",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82405748-adb4-46b5-b967-e8b835f3e8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fb90003-7e68-48b9-b4e5-fe0190e71518",
            "compositeImage": {
                "id": "8c7b2f95-2abc-4b76-8630-c03eb28dfab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82405748-adb4-46b5-b967-e8b835f3e8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d967b995-0040-4538-b1af-8a453859cb03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82405748-adb4-46b5-b967-e8b835f3e8c0",
                    "LayerId": "80947885-c109-4695-8444-2ab92931928f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "80947885-c109-4695-8444-2ab92931928f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fb90003-7e68-48b9-b4e5-fe0190e71518",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}