{
    "id": "f7fe7e10-2794-446d-8d7f-237139a39422",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cc0a361-a463-44db-8292-5d492decbcc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7fe7e10-2794-446d-8d7f-237139a39422",
            "compositeImage": {
                "id": "ad16e906-5488-49aa-85a5-3e810c931c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc0a361-a463-44db-8292-5d492decbcc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "283455fb-0851-43d8-8a22-bdd85216183e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc0a361-a463-44db-8292-5d492decbcc5",
                    "LayerId": "176c5305-dfe3-4a45-913e-e54e5eb68067"
                }
            ]
        },
        {
            "id": "a2d48088-10d1-411e-963b-4e6231db9fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7fe7e10-2794-446d-8d7f-237139a39422",
            "compositeImage": {
                "id": "bdfa570e-ff63-475c-885a-1b499b09191a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2d48088-10d1-411e-963b-4e6231db9fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3781e2c2-bff9-470b-a780-4dd4663c7795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2d48088-10d1-411e-963b-4e6231db9fac",
                    "LayerId": "176c5305-dfe3-4a45-913e-e54e5eb68067"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 128,
    "layers": [
        {
            "id": "176c5305-dfe3-4a45-913e-e54e5eb68067",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7fe7e10-2794-446d-8d7f-237139a39422",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 64
}