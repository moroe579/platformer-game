{
    "id": "f94016b7-3334-434e-844f-7302f193278a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6506c38-fdc0-488a-ac40-552c386353b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f94016b7-3334-434e-844f-7302f193278a",
            "compositeImage": {
                "id": "976ff416-26bb-4886-a161-eb72c69dcb47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6506c38-fdc0-488a-ac40-552c386353b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c74689e-5a54-4ce2-ab71-82cabfc2bd42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6506c38-fdc0-488a-ac40-552c386353b1",
                    "LayerId": "179392bb-2ee1-4f38-92f3-a17da779b380"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "179392bb-2ee1-4f38-92f3-a17da779b380",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f94016b7-3334-434e-844f-7302f193278a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}