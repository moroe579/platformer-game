{
    "id": "409365b9-1787-4afb-b89a-2c5d0a447ac0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoombaR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c7d93ba-cd50-435f-95a8-82c03ead61e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409365b9-1787-4afb-b89a-2c5d0a447ac0",
            "compositeImage": {
                "id": "7cd6e4fd-68f7-4713-8ac0-2ccccf4a9e8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7d93ba-cd50-435f-95a8-82c03ead61e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "539cfce4-b42a-4ed6-8bc3-ebf475ba06f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7d93ba-cd50-435f-95a8-82c03ead61e3",
                    "LayerId": "22e14a21-d4c7-400b-a4ea-7cdce10c213c"
                }
            ]
        },
        {
            "id": "45fe337a-2194-401d-9214-3994696fb5dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409365b9-1787-4afb-b89a-2c5d0a447ac0",
            "compositeImage": {
                "id": "cbb82a97-6e86-487a-bb0c-da3e7ae8136f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45fe337a-2194-401d-9214-3994696fb5dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb95f19a-44b1-4f7b-9956-0dd27ce23a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45fe337a-2194-401d-9214-3994696fb5dd",
                    "LayerId": "22e14a21-d4c7-400b-a4ea-7cdce10c213c"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 64,
    "layers": [
        {
            "id": "22e14a21-d4c7-400b-a4ea-7cdce10c213c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "409365b9-1787-4afb-b89a-2c5d0a447ac0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}