{
    "id": "b605b584-90e3-4846-94e0-90e51d6fd1a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1258251b-1d60-4063-969c-130bda401c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b605b584-90e3-4846-94e0-90e51d6fd1a0",
            "compositeImage": {
                "id": "1f6d68f0-d275-4c27-9ff9-e80e622fe54b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1258251b-1d60-4063-969c-130bda401c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9eadbe-b75e-4382-8c8b-7e4b3d45cc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1258251b-1d60-4063-969c-130bda401c00",
                    "LayerId": "055af049-39b3-4836-8b60-3cc0fa20fa12"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 96,
    "layers": [
        {
            "id": "055af049-39b3-4836-8b60-3cc0fa20fa12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b605b584-90e3-4846-94e0-90e51d6fd1a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}