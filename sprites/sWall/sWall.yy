{
    "id": "00007dde-3bc2-4a99-bd5a-45c97c9bbdbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22d35b4c-f304-41ff-b9ab-c0b98b08996a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00007dde-3bc2-4a99-bd5a-45c97c9bbdbb",
            "compositeImage": {
                "id": "dd90428b-17ad-40dd-9a6d-7b08efe1bc16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d35b4c-f304-41ff-b9ab-c0b98b08996a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bde9f7cd-b624-4a74-b7db-3fd4e4f927fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d35b4c-f304-41ff-b9ab-c0b98b08996a",
                    "LayerId": "d9ab8bca-a66b-4b1f-abc6-4b7043e9cf90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d9ab8bca-a66b-4b1f-abc6-4b7043e9cf90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00007dde-3bc2-4a99-bd5a-45c97c9bbdbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}