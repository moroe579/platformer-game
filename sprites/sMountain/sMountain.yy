{
    "id": "46d3e0f5-c3cf-4621-a2ab-5293a5c50821",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMountain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 110,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "def562bf-1d7c-49cc-8842-3fb8f7a059f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d3e0f5-c3cf-4621-a2ab-5293a5c50821",
            "compositeImage": {
                "id": "b72b054f-863d-4ae3-9217-f266d67664ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def562bf-1d7c-49cc-8842-3fb8f7a059f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53ce85b5-0aef-4262-9e13-bf4ca889fc3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def562bf-1d7c-49cc-8842-3fb8f7a059f0",
                    "LayerId": "03157ad0-a686-4b3d-aca1-02c7e56b919d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "03157ad0-a686-4b3d-aca1-02c7e56b919d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46d3e0f5-c3cf-4621-a2ab-5293a5c50821",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": -172,
    "yorig": 242
}