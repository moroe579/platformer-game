{
    "id": "e1d131d8-1ac5-48a4-b66d-fbebefafb1c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "175ae534-28f6-4743-9fda-222473f9ff91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d131d8-1ac5-48a4-b66d-fbebefafb1c2",
            "compositeImage": {
                "id": "e3a9ff13-6910-490c-adcf-71eced8249c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "175ae534-28f6-4743-9fda-222473f9ff91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ed2763-7cc6-472d-b79c-0d6ab07695d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "175ae534-28f6-4743-9fda-222473f9ff91",
                    "LayerId": "6755155d-1696-42da-adfc-6e7fb68a37ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "6755155d-1696-42da-adfc-6e7fb68a37ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1d131d8-1ac5-48a4-b66d-fbebefafb1c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}