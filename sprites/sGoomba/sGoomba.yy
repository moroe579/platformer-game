{
    "id": "85bf3e70-076d-45d8-9e7f-b159899ba73f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoomba",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4708396-5af6-44d4-a8c9-f649690615dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85bf3e70-076d-45d8-9e7f-b159899ba73f",
            "compositeImage": {
                "id": "28435661-c73b-45cf-9298-b81b704ad74e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4708396-5af6-44d4-a8c9-f649690615dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0567fd4d-cc8c-4e2b-a72a-f21286b8a918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4708396-5af6-44d4-a8c9-f649690615dc",
                    "LayerId": "cff53e4f-07d2-404a-89ff-1e7528ff802f"
                }
            ]
        },
        {
            "id": "9929613c-dcd4-4142-8864-72c35d6f7efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85bf3e70-076d-45d8-9e7f-b159899ba73f",
            "compositeImage": {
                "id": "00b817c3-de9c-45a6-b587-e3f4532d01bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9929613c-dcd4-4142-8864-72c35d6f7efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40620414-c4c8-4e12-9487-ec286da031f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9929613c-dcd4-4142-8864-72c35d6f7efc",
                    "LayerId": "cff53e4f-07d2-404a-89ff-1e7528ff802f"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 64,
    "layers": [
        {
            "id": "cff53e4f-07d2-404a-89ff-1e7528ff802f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85bf3e70-076d-45d8-9e7f-b159899ba73f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}