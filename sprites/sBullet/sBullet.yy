{
    "id": "46b63632-714c-41a6-826c-ab9bee99764a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 33,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "451f755f-ba72-4e9a-aae7-c7ae3f3ccaa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b63632-714c-41a6-826c-ab9bee99764a",
            "compositeImage": {
                "id": "ecec6174-08eb-4dbc-92c8-1d24c61be570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451f755f-ba72-4e9a-aae7-c7ae3f3ccaa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a23931-8e8e-4266-ad7b-89034fbac42e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451f755f-ba72-4e9a-aae7-c7ae3f3ccaa6",
                    "LayerId": "0e2600bb-0ffb-4a0e-ba55-73f4d7c788bd"
                }
            ]
        },
        {
            "id": "4d6ec656-f7ef-4e96-a624-ae960651b539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b63632-714c-41a6-826c-ab9bee99764a",
            "compositeImage": {
                "id": "68b6bb92-da82-4106-a1df-f7fdb371dce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d6ec656-f7ef-4e96-a624-ae960651b539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a16cd3-7fc2-4efa-8a40-41fae5b95c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d6ec656-f7ef-4e96-a624-ae960651b539",
                    "LayerId": "0e2600bb-0ffb-4a0e-ba55-73f4d7c788bd"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 18,
    "layers": [
        {
            "id": "0e2600bb-0ffb-4a0e-ba55-73f4d7c788bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46b63632-714c-41a6-826c-ab9bee99764a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}