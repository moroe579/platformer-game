{
    "id": "cef52cff-583b-4f07-8fb4-ad071ef65abf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoombaD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 48,
    "bbox_top": 47,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8715a0fe-fbf7-459d-bfe8-15dcdffbd922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef52cff-583b-4f07-8fb4-ad071ef65abf",
            "compositeImage": {
                "id": "d7d6f69e-d823-4416-a8a3-ae92b472d733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8715a0fe-fbf7-459d-bfe8-15dcdffbd922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ab22d35-c76c-4bf8-bd0f-bc09bd2c05e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8715a0fe-fbf7-459d-bfe8-15dcdffbd922",
                    "LayerId": "1542d492-c662-43c0-b832-6b9c9c7b7d0a"
                }
            ]
        },
        {
            "id": "92f15938-8bf8-4b45-a963-d8a16e461422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef52cff-583b-4f07-8fb4-ad071ef65abf",
            "compositeImage": {
                "id": "11ccebac-ad83-4983-9a61-68a1b1470c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92f15938-8bf8-4b45-a963-d8a16e461422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "065b1ea4-1a0b-4d0f-8bcc-9a5af88b01d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92f15938-8bf8-4b45-a963-d8a16e461422",
                    "LayerId": "1542d492-c662-43c0-b832-6b9c9c7b7d0a"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 64,
    "layers": [
        {
            "id": "1542d492-c662-43c0-b832-6b9c9c7b7d0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cef52cff-583b-4f07-8fb4-ad071ef65abf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -27,
    "yorig": 39
}