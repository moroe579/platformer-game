{
    "id": "58b25b56-3212-4d41-91a8-396730037bd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fedf0b4-3001-402a-8f1a-96ce98a025d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58b25b56-3212-4d41-91a8-396730037bd9",
            "compositeImage": {
                "id": "d60f0846-5d57-44e3-ba02-d144131709e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fedf0b4-3001-402a-8f1a-96ce98a025d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f071e21-364c-4c28-94b5-04c6a5a2d573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fedf0b4-3001-402a-8f1a-96ce98a025d3",
                    "LayerId": "31c1bc02-d116-4dbd-8a6f-8cb6749faf65"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 48,
    "layers": [
        {
            "id": "31c1bc02-d116-4dbd-8a6f-8cb6749faf65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58b25b56-3212-4d41-91a8-396730037bd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 34,
    "yorig": 16
}