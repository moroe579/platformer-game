{
    "id": "34763fb5-b2c8-44ad-b225-62bf992ac890",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTransition",
    "eventList": [
        {
            "id": "05e09c14-6457-4277-b28e-b942f33cceac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34763fb5-b2c8-44ad-b225-62bf992ac890"
        },
        {
            "id": "afac1793-94f1-4dd4-b583-b540ac692265",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "34763fb5-b2c8-44ad-b225-62bf992ac890"
        },
        {
            "id": "00ab44fe-333c-4dd9-89e7-14b452cf63ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "34763fb5-b2c8-44ad-b225-62bf992ac890"
        },
        {
            "id": "df82a5a7-1e58-4619-943c-57a7cbe8607e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "34763fb5-b2c8-44ad-b225-62bf992ac890"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}