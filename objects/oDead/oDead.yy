{
    "id": "0a071850-e292-494f-bbfe-b34149a69428",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDead",
    "eventList": [
        {
            "id": "d0b2f182-fe00-41aa-b884-511aba5e3905",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0a071850-e292-494f-bbfe-b34149a69428"
        },
        {
            "id": "5f2f1ad4-2278-4e21-a498-9a037a1d1423",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a071850-e292-494f-bbfe-b34149a69428"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cef52cff-583b-4f07-8fb4-ad071ef65abf",
    "visible": true
}