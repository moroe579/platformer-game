{
    "id": "5a94680b-cf61-4063-886b-446cae2c73b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevelEnd",
    "eventList": [
        {
            "id": "faa54c22-ec85-4066-bf73-16e14dad771e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1154c449-ded4-4ec2-a8f8-2524ba73adcf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5a94680b-cf61-4063-886b-446cae2c73b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1fb90003-7e68-48b9-b4e5-fe0190e71518",
    "visible": false
}