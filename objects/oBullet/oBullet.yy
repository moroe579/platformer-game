{
    "id": "e92ea7ec-ca12-43b4-a564-02e35daba484",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet",
    "eventList": [
        {
            "id": "5bf70d40-d3cd-4908-973a-5ced1dace144",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e92ea7ec-ca12-43b4-a564-02e35daba484"
        },
        {
            "id": "d2b4c816-b8ac-4dd2-8b10-56a85bd6ec65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "e92ea7ec-ca12-43b4-a564-02e35daba484"
        },
        {
            "id": "af3bf016-77d2-4d45-86ff-3bc28c1b3fca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9756f3d-aa85-4115-9d3c-36f186bb3dae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e92ea7ec-ca12-43b4-a564-02e35daba484"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46b63632-714c-41a6-826c-ab9bee99764a",
    "visible": true
}