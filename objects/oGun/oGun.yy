{
    "id": "bd449ce0-7595-492b-8d12-c9fcb60cea3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGun",
    "eventList": [
        {
            "id": "85ac2ce8-7f11-4681-888a-8ce84d1fd82b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "bd449ce0-7595-492b-8d12-c9fcb60cea3d"
        },
        {
            "id": "8e255533-8635-488c-a524-4f962f72ae04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd449ce0-7595-492b-8d12-c9fcb60cea3d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58b25b56-3212-4d41-91a8-396730037bd9",
    "visible": true
}